#!/usr/bin/env bash

set -eux

# Usage: ./build.sh ${VERSION}

PROJECT_NAME='trunk'
readonly PROJECT_NAME
PUBLIC_DIR="$(pwd)/public"
readonly PUBLIC_DIR
VERSION="${1}"
readonly VERSION
TARGET='x86_64-unknown-linux-musl'

if [[ ! -d "${PROJECT_NAME}" ]]; then
    REPOSITORY='https://github.com/thedodd/trunk'
    readonly REPOSITORY

    git clone "${REPOSITORY}"
fi

cd "${PROJECT_NAME}" || exit 1

git switch --detach ${VERSION}
mkdir ../public/${VERSION}

cargo build --release --target ${TARGET}

cd "target/${TARGET}/release/" || exit 1

tar acvf ${PROJECT_NAME}-${TARGET}.tar.gz ${PROJECT_NAME}
tar acvf ${PROJECT_NAME}-${TARGET}.tar.xz ${PROJECT_NAME}

for file in *.tar.*; do
    sha256sum "${file}" > "${file}".sha256
    b2sum "${file}" > "${file}".b2
done

cp ./*.tar.* "${PUBLIC_DIR}/${VERSION}"
