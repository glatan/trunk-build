NAME = trunk-build
WORKDIR = /workdir

.PHONY: p.build
p.build:
	@podman build -t ${NAME} .

build.%:
	-@podman run --name ${@} -v .:${WORKDIR} -w ${WORKDIR} -it ${NAME} ./build.sh $*
	@podman rm ${@}

.PHONY: run.bash
run.bash:
	-@podman run --name ${@} -v .:${WORKDIR} -w ${WORKDIR} -it ${NAME} bash
	@podman rm ${@}
