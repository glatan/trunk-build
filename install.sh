#!/usr/bin/env bash

set -eux

pacman -Syyu --noconfirm
pacman -S musl rustup git gcc --noconfirm

rustup default stable
rustup target add x86_64-unknown-linux-musl
