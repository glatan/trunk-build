FROM docker.io/archlinux:latest

ENV CARGO_HOME '/cargo'

WORKDIR /workdir

COPY install.sh .

RUN ./install.sh
